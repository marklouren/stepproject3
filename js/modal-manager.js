import { LocalStorageManager } from "./local-storage-manager.js";
import { request } from './requests.js'
import { VisitManager } from "./visit-manager.js";

class Visit {
    constructor(doctor, visitPurpose, visitDate, patientName) {
        this.doctor = doctor;
        this.visitPurpose = visitPurpose;
        this.visitDate = visitDate;
        this.patientInfo = patientName;

    }
}

class Cardiologist extends Visit {
    constructor(
        doctor,
        visitPurpose,
        visitDate,
        patientName,
        normalBloodPressure,
        bodyIndex,
        pastIllnesses,
        age,
        comment
) {
        super(doctor, visitPurpose, visitDate, patientName);
        this.normalBloodPressure = normalBloodPressure;
        this.bodyIndex = bodyIndex;
        this.pastIllnesses = pastIllnesses;
        this.age = age;
        this.comment = comment
    }

}

class Dentist extends Visit {
    constructor(
        doctor,
        visitPurpose,
        visitDate,
        patientName,
        lastVisitDate,
        comment
    ) {
        super(doctor, visitPurpose, visitDate, patientName);
        this.lastVisitDate = lastVisitDate;
        this.comment = comment
    }
}
class Therapist extends Visit{
    constructor(
        doctor,
        visitPurpose,
        visitDate,
        patientName,
        age,
        comment
    ) {
        super(doctor, visitPurpose, visitDate, patientName);
        this.age = age;
        this.comment = comment
    }
}

/**
 *
 * @class ModalManager
 * @classdesc Class provides methods for opening and closing modal window as well as gathering data
 *
 * */

class ModalManager {
    createButton = document.getElementsByClassName('card-header-button')[0];
    cardModalWindow = document.getElementsByClassName('card-section-modal')[0];
    closeModalWindow = document.getElementsByClassName('card-section-modal-close')[0];
    closeModalWindowBtn = document.getElementsByClassName('card-section-modal-form-buttons-close')[0];

    selectDoctor = document.getElementsByClassName('card-section-modal-form-list-doctors')[0]; // <select>
    listChoiceFields = document.getElementsByClassName('card-section-modal-form-choice');
    listFields = Array.prototype.slice.call(this.listChoiceFields);  // [<div><input></div>, <div><input></div>, <div><input></div>]
    modalInputFields=document.getElementsByClassName('card-section-modal-form-input');
    listCardInputs = Array.prototype.slice.call(this.modalInputFields);// [ <input>, <input> ]

    form = document.getElementsByClassName('card-form')[0]; // <form>



    constructor(){
        ModalManager.showModal(this.createButton, this.cardModalWindow);
        ModalManager.hideModal(this.closeModalWindow,this.cardModalWindow);
        ModalManager.hideModalBtn(this.closeModalWindowBtn,this.cardModalWindow);
        ModalManager.hideModalOutside(this.createButton, this.cardModalWindow);
        this.changeFields();
        this.gatherFormValues();
        this.switchDoctorFields('cardiologist');


    }

    static showModal(btn, modal){
        btn.addEventListener('click', ()=>{
            modal.style.display = 'flex';

        })
    }
    static hideModal(btn, modal){
        btn.addEventListener('click', ()=>{
            modal.style.display = 'none'
        })
    }
    static hideModalBtn(btn, modal){
        btn.addEventListener('click', ()=>{
            modal.style.display = 'none';
        })
    }
    // Hide Modal by Click outside the window
    static hideModalOutside(btn,modal) {

        modal.addEventListener('click', function(event){
            event.stopPropagation();
        });
        document.addEventListener('click', (event) => {
          //  console.log(modal.children);
            if ( modal.style.display==='flex'&&event.target !== modal&&event.target !== btn) {
               modal.style.display = 'none';
            }
        })
    }

    /**
     * @method switchDoctorFields - Checks <select> value (e.g. cardiologist) and changes Input fields
     *
     * */
     switchDoctorFields(doctorType){
        this.listFields.forEach(input => {
            input.style.display='none';
            if (input.classList.contains(doctorType)) {
                input.style.display='flex';
            }
        })
    }
    /**
     * @method changeFields - On change <select> event fields are changing
     *
     * */
    changeFields() {
        this.selectDoctor.addEventListener('change', () => {
            let doctor =  this.selectDoctor.value;
            this.switchDoctorFields(doctor)
        });
    }

    /**
     * @method afterSubmit - Hide Modal and clear Fields Helper for _validateCardObject
     *
     * */
    afterSubmit(){

        this.cardModalWindow.style.display = 'none';
        this.listCardInputs.forEach(input => input.value = '');
    }


    textAreaValidation() {
        if (this.listCardInputs[this.listCardInputs.length-1].value === ''){
            this.listCardInputs[this.listCardInputs.length-1].value = 'No Comments';
        }
    }


    gatherFormValues(){
        let visit;
        this.form.addEventListener('submit', (event) => {
            event.preventDefault();
            this.textAreaValidation();
                switch(this.selectDoctor.value) {
                    case 'cardiologist': {

                        visit = new Cardiologist(
                            "Кардиолог",
                            document.querySelector('input[name="visit-goal"]').value,
                            document.querySelector('input[name="visit-date"]').value,
                            document.querySelector('input[name="credentials"]').value,
                            document.querySelector('input[name="normal-blood-pressure"]').value,
                            document.querySelector('input[name="body-index"]').value,
                            document.querySelector('textarea[name="past-illnesses"]').value,
                            document.querySelector('input[name="age"]').value,
                            document.querySelector('textarea[name="comments"]').value
                        );
                        break;
                    }
                    case 'dentist': {

                        visit = new Dentist(
                            "Стоматолог",
                            document.querySelector('input[name="visit-goal"]').value,
                            document.querySelector('input[name="visit-date"]').value,
                            document.querySelector('input[name="credentials"]').value,
                            document.querySelector('input[name="last-visit-date"]').value,
                            document.querySelector('textarea[name="comments"]').value,
                        );
                        break;
                    }
                    case 'therapist': {

                        visit = new Therapist(
                            "Терапевт",
                            document.querySelector('input[name="visit-goal"]').value,
                            document.querySelector('input[name="visit-date"]').value,
                            document.querySelector('input[name="credentials"]').value,
                            document.querySelector('input[name="age"]').value,
                            document.querySelector('textarea[name="comments"]').value,
                        );
                        break;
                    }
                }

            request.createCard(request.registration(), visit);// add visit object to DB
            LocalStorageManager.pushToLocalStorage(`${String(+new Date())}`, visit);   // add visit object to LS
            this.afterSubmit();
        });

    }



}

const modalWindow = new ModalManager();

export { ModalManager, modalWindow };

