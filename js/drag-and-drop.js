export class DragAndDrop {
    constructor() {

    }

    static makeDragAndDrop() {
        let items = document.getElementsByClassName('card-section-card');
        let lists = document.getElementsByClassName('card-list');


        let draggedItem = null;

        for(let i = 0; i < items.length; i++) {

            let item = document.getElementsByClassName('card-section-card')[i];

            item.addEventListener('dragstart', function() {

                draggedItem = item;
                setTimeout(function() {
                    item.style.display = 'none';
                }, 0)

            });

            item.addEventListener('dragend',function() {

                item.style.display = 'flex';
                setTimeout(function() {
                    draggedItem = null;
                }, 0)
            });


            for (let j = 0; j < lists.length; j++) {
                let list = lists[j];

                list.addEventListener('dragover', function(event) {
                    event.preventDefault();
                });

                list.addEventListener('dragenter', function() {
                    list.style.backgroundColor =  'rgba(0,0,0,.6)';
                });

                list.addEventListener('dragleave', function() {
                    list.style.backgroundColor =  'rgba(0,0,0,.4)';
                });


                list.addEventListener('drop', function() {
                    list.style.backgroundColor =  'rgba(0,0,0,.4)';
                    list.append(draggedItem);
                })

            }

        }
    }
}
