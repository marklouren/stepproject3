import {LocalStorageManager} from "./local-storage-manager.js";
import { request } from "./requests.js";


export class VisitManager {

    // CREATE CARD VARIABLES
    cardSection = document.getElementsByClassName('card-section')[0];
    cardsList = document.getElementsByClassName("card-section-card");
    informMessageBox = document.getElementsByClassName('card-section-empty-text')[0];
    form = document.getElementsByClassName('card-form')[0];
    lists = document.getElementsByClassName('card-list');
    // id;
    constructor(){
        // this.showMoreCardInfo();
        // this.updatePage();
        // LocalStorageManager.removeFromLocalStorage();
    }

    displayEmptyNotification(cards){
        if (cards.length > 0){
            this.informMessageBox.style.display = 'none';
        } else {
            this.informMessageBox.style.display = 'flex';
        }
    }

    showMoreCardInfo(){
        let  listCards=[].slice.call(this.cardsList);
        listCards.forEach( card => {
            card.addEventListener('click', function(event){
                if (event.target.classList.contains('card-section-card-button-see-more')){
                    if (this.children[2].classList.contains('hide')){
                        this.children[2].classList.remove('hide');
                        event.target.innerText='Cкрыть';
                    } else {
                        this.children[2].classList.add('hide');
                        event.target.innerText='Показать Больше';
                    }
                }
            })
        });
    }

    // REMOVE CARDS FROM THE DESK and DB
    deleteCardArrow(cardsFromServer){
        let closeArrowArray = [].slice.call(document.getElementsByClassName('card-section-card-close-button'));
        // console.log('Here is server info:', cardsFromServer);
        // console.log('Here is:', closeArrowArray);
        closeArrowArray.forEach( (item) => {
            item.addEventListener('click', function(){
                item.parentNode.remove();
                cardsFromServer.forEach(obj => {
                    if (obj.id === item.dataset.field) {
                        request.deleteCard(request.registration(), item.dataset.field)
                    }
                })

                // LocalStorageManager.removeFromLocalStorage(item.dataset.id);
            })
        })
    }
    deleteCardBtn(cardsFromServer){
        let deleteBtnArray = [].slice.call(document.getElementsByClassName('card-section-card-button-delete'));
        deleteBtnArray.forEach( (item) => {
            item.addEventListener('click', function(){
                // let card = document.querySelectorAll(`[data-field="${item.dataset.field}"]`)[0];
                item.parentNode.parentNode.remove();
                cardsFromServer.forEach(obj => {
                    if(obj.id === item.dataset.id) {

                        request.deleteCard(request.registration(), item.dataset.id)
                    }
                })
                // LocalStorageManager.removeFromLocalStorage(item.dataset.id);
            })
        })
    };


    // Helper for Cart Creation Method
    static doctorVariableFields(doctor, object){
        if (doctor === 'Стоматолог'){
            return `<div class="card-section-card-field">
                <span class="card-section-card-field-desc">Дата Последнего визита: </span>
                <span class="card-section-card-text" data-field="last-visit-date">${object.lastVisitDate}</span>
            </div>`}
        if (doctor === 'Терапевт'){
            return ` <div class="card-section-card-field">
                <span class="card-section-card-field-desc"> Ваш возраст: </span>
                <span class="card-section-card-text" data-field="age">${object.age}</span>
            </div> `
        }
        if (doctor === 'Кардиолог'){
            return ` <div class="card-section-card-field">
                <span class="card-section-card-field-desc">Цель Визита: </span>
                <span class="card-section-card-text" data-field="visit-goal">${object.visitPurpose}</span>
            </div>
            <div class="card-section-card-field"
                <span class="card-section-card-field-desc">Обычное давление: </span>
                <span class="card-section-card-text" data-field="normal-blood-pressure">${object.normalBloodPressure}</span>
            </div>

            <div class="card-section-card-field">
                <span class="card-section-card-field-desc">Индекс Массы Тела: </span>
                <span class="card-section-card-text" data-field="body-index">${object.bodyIndex}</span>
            </div>

            <div class="card-section-card-field">
                <span class="card-section-card-field-desc">Перенесенные заболевания: </span>
                <span class="card-section-card-text" data-field="past-illnesses">${object.pastIllnesses}</span>
            </div>
            <div class="card-section-card-field">
                <span class="card-section-card-field-desc"> Ваш возраст: </span>
                <span class="card-section-card-text" data-field="age">${object.age}</span>
            </div> `
        }
    }


    displayCards(cardsFromServer){

        // console.log('Cards from server:',cardsFromServer);

        this.displayEmptyNotification(cardsFromServer);

            cardsFromServer.forEach(obj => {
                let cardItem = document.createElement('div');
                cardItem.setAttribute('draggable', 'true');
                cardItem.classList.add('card-section-card');
                cardItem.innerHTML = `
        <!-- Visible Fields -->
             <div class="card-section-card-close-button" data-field="${obj.id}">x</div>
             <div class="card-section-card-visible">
                 <div class= "card-section-card-field">
                    <span class="card-section-card-field-desc">ФИО: </span>
                    <span class="card-section-card-text" data-field="credentials">${obj.patientInfo}</span>
                </div>
                 <div class= "card-section-card-field">
                    <span class="card-section-card-field-desc">Дата Визита: </span>
                    <span class="card-section-card-text" data-field="credentials">${obj.visitDate}</span>
                </div>
                <div class="card-section-card-field">
                    <span class="card-section-card-field-desc">Доктор: </span>
                    <span class="card-section-card-text" data-field="doctor">${obj.doctor}</span>
                </div>
            </div>
        <!-- Hidden Fields -->
              <div class="card-section-card-hidden hide">
              ${VisitManager.doctorVariableFields(obj.doctor, obj)}
               <div class="card-section-card-field">
                    <span class="card-section-card-field-desc">Комментарии: </span>
                    <span class="card-section-card-text" data-field="comments">${obj.comment}</span>
                </div> 
            </div>
            <!-- Buttons-->
              <div class="card-section-card-button">
                <button class="card-section-card-button-see-more card-btn">Показать Больше</button>
                <button class="card-section-card-button-delete card-btn " data-id="${obj.id}">Удалить</button>
            </div>
                `;

                this.lists[Math.floor(Math.random() * 3)].append(cardItem);

            });
    }

    // IN ORDER TO GET UPDATES FROM LOCAL STORAGE
    updatePage(){
           location.reload();
    }

}

export let visitManager = new VisitManager();


