/**
 *
 * @class LocalStorageManager
 * @classdesc Class provides methods for sending and receiving items to Local Storage.
 *
 * */

export class LocalStorageManager {

    /**
     * Converts value to json format.
     * Sets Item in LS
     *
     * @param {string} key - key to store item in Local Storage
     * @param {Object[] || []} value - Value which is stored.
     */
    static pushToLocalStorage(key, value) {
        const jsonValue = JSON.stringify(value);
        localStorage.setItem(`${key}`, `${jsonValue}`);
    }

    /**
     * Receives item from LS
     * Converts value back from json format.
     *
     * @param {string} key - key to get item from Local Storage
     */
    static getFromLocalStorage(key) {
        const receivedValue =  localStorage.getItem(`${key}`);
        return JSON.parse(receivedValue);
    }
    static removeFromLocalStorage(key){
        localStorage.removeItem(key);
    }
}


