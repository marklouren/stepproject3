// v@web3.agency
// kozadereza
// token c5f0433511b2
import {LocalStorageManager} from "./local-storage-manager.js";
import { visitManager } from "./visit-manager.js";
import { DragAndDrop } from "./drag-and-drop.js";

class Request{

    constructor(){
        this.getCards(this.registration());

    }

    registration(){
        let xhr = new XMLHttpRequest();
        const data =  { email: 'v@web3.agency', password: "kozadereza"};
        xhr.open("POST", "http://cards.danit.com.ua/login", false);
        xhr.setRequestHeader('Content-type', "application/x-www-form-urlencoded");

        xhr.onreadystatechange = function(){
            if (xhr.readyState === 4 && xhr.status === 200){
                return JSON.parse(xhr.response).token
            }
        };
        xhr.send(JSON.stringify(data));
        return JSON.parse(xhr.response).token;

    }


    createCard (token, data){
        fetch('http://cards.danit.com.ua/cards', {
            method: 'post',
            headers: { "Content-Type": "application/json",
                Authorization: `Bearer ${token}`},
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(json => {

                console.log(json.id);

            })
            .then(json => {
                visitManager.updatePage();
            })

    }

    deleteCard(token, cardID) {
        fetch(`http://cards.danit.com.ua/cards/${cardID}`, {
            method: 'delete',
            headers: { "Content-Type": "application/json",
                Authorization: `Bearer ${token}`},
        })
            .then(response => response.json())
            .then(result => {
                console.log('Deleted:', result);

        })
    }

    getCards(token){

        fetch('http://cards.danit.com.ua/cards', {
            method: 'get',
            headers: { "Content-Type": "application/json",
                Authorization: `Bearer ${token}`},
        })
            .then(response=>
            response.json())
            .then(json => {
                    visitManager.displayCards(json);
                    visitManager.deleteCardArrow(json);
                    visitManager.deleteCardBtn(json);
                    visitManager.showMoreCardInfo();
                })
            .then(json => {
                DragAndDrop.makeDragAndDrop();
            })

    }


    editCard(token, cardID, data) {
        fetch(`http://cards.danit.com.ua/cards/${cardID}`, {
            method: 'put',
            headers: { "Content-Type": "application/json",
                Authorization: `Bearer ${token}`},
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(result => {
                console.log(result);

            })
    }



}

export const request = new Request();